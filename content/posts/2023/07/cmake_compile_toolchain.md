---
title: "CMake Compile Toolchain"
date: 2023-07-28T12:25:00-04:00
lastmod: 2023-07-29T11:55:55-04:00
tags: ["cmake", "cpp", "tool"]
categories: ["cmake", "writing"]
draft: false
toc: true
---

> CMake is an open-source, cross-platform family of tools designed to build, test and package software.Cmake is used to control the software compliation process using simple platform and compiler independent configuration files, and generate native makefiles and workspaces that cna be used in the compiler environment of your choice.

<a id="code-snippet--Simple-Cmake-Work"></a>
```shell
# Assume in Project Root
cmake -S. -Bbuild
cmake --build build [--target docs]
ctest --test-dir=build -C Debug -VV # Use ctest for project's test.
```


## Organized Project {#organized-project}


### Simple Demo For Cpp Work {#simple-demo-for-cpp-work}

<a id="code-snippet--CMakeLists.txt"></a>
```shell
cmake_minimium_required(VERSION 3.27.0 FATAL_ERROR)

project(CppDemo)

add_executable(CppDemo main.cpp)
```


## Dependency Manager {#dependency-manager}


### `CPM` Dependency Manager {#cpm-dependency-manager}


## Custom Work in `CMakeLists.txt` {#custom-work-in-cmakelists-dot-txt}
