---
title: "Hugo in Org-Mode"
date: 2023-07-17T13:32:00-04:00
lastmod: 2023-07-29T11:57:19-04:00
tags: ["note", "hugo"]
categories: ["hugo", "writing"]
draft: false
toc: true
---

> If you want to use hugo and work in org-mode, Follow the doc.


## Hugo + Org-Mode? {#hugo-plus-org-mode}

```bash
sudo apt install emacs hugo -y
# Erro will appeal while you run this command in Ubuntu, you would
# better check your repo and run it again.
```


### Org-Mode Config {#org-mode-config}

`init-org-hugo.el` would be like:

```elisp
;;; init-org-hugo.el --- Configure org-mode-ox-hugo global behaviour -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require-package 'ox-hugo)


(with-eval-after-load 'ox
  (require 'ox-hugo))

(provide 'init-org-hugo)
;;; init-org-hugo.el ends here
```
