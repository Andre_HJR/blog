#+OPTIONS: author:nil ^:{}
#+startup: indent
#+hugo_front_matter_format: yaml
#+HUGO_BASE_DIR: ../../../
#+HUGO_SECTION: posts/2023/07
#+DATE: [2023-07-29 Sat 01:28]
#+HUGO_CUSTOM_FRONT_MATTER: :toc true
#+HUGO_AUTO_SET_LASTMOD: t
#+HUGO_TAGS: qt cpp
#+HUGO_CATEGORIES: qt writing
#+HUGO_DRAFT: false
#+TITLE: Qt Crossing Finger

* Basic Knownledge

** About ~Q_OBJECT~

The ~Q_OBJECT~ macro at the begining of the class definition is necessary for all classes that define ssignals or slots.


*** ~Signal~ and ~Slots~

The ~signal~ and ~slots~ mechanism is the core of ~QT~, and to be proficient in ~QT~ work, Understanding of the ~signal~ and ~slot~ is required.

~Signals~ and ~Slots~ are high-level interfaces for communication between objects, which are a core feature of ~QT~ and important differentiator between ~QT~ and other ~Cpp-Tookits~. Both of them are ~QT~'s self defined communication mechanism that is independent of standard ~C/C++~ language, so to properly handle ~signal~ and ~slots~, Meta Object Compiler (~MOC~) is required. ~MOC~ is a cpp preprocessor that automatically generates the additional code needed for high-level event processing. In many GUI toolkits that we know, widgets have a callback function in response to each action they cna trigger, usually a pointer to a function. While ~signals~ and ~slots~ replace the callback function (lots of time in declare of function you need identify the function pointer of callback function), making it easier for us to write the communication programs. The ~signal~ and ~slot~ can carry any number and type of arguments, they are completely safe and do not generate core dumps like callback functions.

All classes derived from ~QObject~ or its sub-classes (like ~QWidget~) can contain ~signal~ and ~slot~. When an Object changes its state, the signal is emitted by the Object, and that's all the Object does, it doesn't know is receiving the ~signal~ on the other end. This is true information encapsulation, which ensures that the Object is used as a real software component. ~Slot~ is uesd to receive ~signal~, but both of them are ordinary object member functions. A ~slot~ does not know if any ~signal~ is connected to it. Moreover, the Object does not understand the specific communication mechanism. It is possible to connect many signals to a single ~slot~, to connect a single ~signal~ to many slots, or even to connect one ~signal~ to another, and the system will immediately transmit the second signal whenever the first ~signal~ is transmitted.

In Summary, ~signal~ and ~slot~ consititute a powerful component programming mechanism.

**** Observer

Observer is a behavioral design pattern that lets you define a subscription mechanism to notify multiple objects about any events that happend to the object they're observing.

#+BEGIN_SRC cpp
  struct IObserver {
    virtual ~IObserver() {};
    virtual void Update(const std::string &messageFromSubject) = 0;
  };

  struct ISubject {
    virtual ~ISubject() {};
    virtual void Attach(IObserver *observer) = 0;
    virtual void Detach(IObserver *observer) = 0;
    virtual void Notify() = 0;
  };

  struct Subject: ISubject final {
    virtual ~Subject() {
      std::cout << "Goodbye, I was the Subject.\n";
    }

    void Attach(IObserver *observer) override {
      _listObserver.push_back(observer);
    }
    void Attach(IObserver *observer) override {
      _listObserver.remove(observer);
    }
    void Notify() override {
      std::list<IObserver*>::iterator iterator = _listObserver.begin();
      HowManyObserver();
      for(;iterator != listObserver.end();
          (*iterator)->Update(message), ++iterator) ;
    }

    void CreateMessage(std::string message = "Empty") {
      _message = message;
      Notify();
    }

    void HowManyObserver() {
      std::cout << "There are " <, _listObserver.size() << " observers in the list.\n";
    }

    void SomeBusinessLogic() {
      _message = "change message message";
      Notify();
      std::cout << "I'm about to do some thing important.\n";
    }
  private:
    std::list<IObserver*> _listObserver;
    std::string _message;
  };

#+END_SRC


* UI Compent

** QLineEdit

#+BEGIN_SRC cpp
  QString str = ""; /**! str: RegExp Pattern, for validating.*/
  QLineEdit lineEdit; /**! lineEdit: Instantiate Component of QLineEdit*/
  lineEdit.setValidator(new QRegExpValidator(QRegExp(str)));
#+END_SRC
